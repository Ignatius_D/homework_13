﻿#include <iostream>
#include "Helpers.h"

int main()
{
    int a, b;
    std::cout << "Enter first number: " << std::endl;
    std::cin >> a;
    std::cout << "Enter second number: " << std::endl;
    std::cin >> b;
    std::cout << "Result: " << squareSum(a, b) << std::endl;
    return 0;
}